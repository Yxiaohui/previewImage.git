# previewImage
## 介绍
在使用uni-app框架的时候，需要用到图片预览功能，框架提供了相应的[uni.previewImage](https://uniapp.dcloud.io/api/media/image?id=unipreviewimageobject)方法。
但是在某些特殊情况下该方法无法满足需求，如：
* 需要对预览界面进行更多样式上的调整时。
* 需要当前页面不被隐藏时。

**例如**
1. 我们需要显示图片的描述信息。
2. 我们要做的一个在线学习功能，当用户离开学习页面就要停止计时。
当用户点击图片预览查看时，也算做正常计时。

> 在调用uni的预览方法会新开一个页面，导致当前页面被隐藏，触发onHide事件。
>从而影响到了onHide事件的处理业务。
>我查阅了uni.previewImage方法参数，没有找到可以指定使用非原生预览的设置方式。
>才决定自定义一个图片预览。

整个插件的内容很少，只要稍作整理，大家都能自己写一个。
我这边把代码贡献出来，希望给大家争取点耍朋友的时间。

**此插件的特性**
* 多端支持（无dom操作，跨全端:安卓、苹果、微信小程序、h5）
* 手势缩放（放大图片查看细节）
* 描述文字（可显示当前图片的说明描述信息）
* 长按事件（保存图片，识别图片中的二维码等功能）
* 样式自定义（示例代码有样式调试页面，方便调试）
* 支持图片旋转
* 自带保持图片功能（h5端的图片保存需要同域，否则在新页面打开图片;app端不支持本地路径）

**注意**
* 此插件的流畅程度赶不上官方的[uni.previewImage](https://uniapp.dcloud.io/api/media/image?id=unipreviewimageobject)方法,只是因为官方的方法无法满足需求才被迫这样子搞。
* 缩放时默认从中心放大，手指移动图片时又可能会出发切换事件，整体体验一般。  
	* 计划优化中，已经有优化思路了，只有移动图片到头了才触发切换事件或者图片缩放比例为1时才触发切换操作。
	* 但是测试发现整个过程不是很流畅，容易误判，目前暂时使用reduction属性手动控制。
	* 当reduction="true"时，用户放大图片后必须缩小到原大小才能滑动切换到下一张图。
* 因为image组件mode属性+宽度写死为了100%的原因，目前的放大操作无法基于原图进行放大，而是对视图进行了放大，所以放大后内容是模糊的(无论图片多高清，缩放功能就是个摆设)。
	* 正在考虑优化方案,如将image的宽度默认设置为400%后再缩放为100%大小。
* 官方的方法实在不能满足需求了，再考虑使用此类插件。
* HBuilderX V2.8.11.20200907版本已增加H5端图片预览缩放功能。

>本人会使用空闲时间对此插件进行长期优化维护。
>如果大家有什么建议或发现bug都可在评论区或邮箱联系我。

## 示例项目结构
+ components `插件目录`
	+ previewImage `图片预览插件`
		- previewImage.vue `插件源码`
+ pages `页面目录`
	+ index `首页`
		- index.vue `包含插件的使用示例`
	+  style `样式`
		- style.vue `可在此页面高效定制好样式，再修改插件文件` 
+ static `静态资源`
	- a/b/c/d.jpg `调试用的图片`

## 组件属性

|	属性	|	类型	|	必填	|	默认	|	说明	|
|--|--|--|:--|:--|
|imgs	|数组	|	是	|空		|图片地址数组|
|descs	|数组	|	否	|空		|图片描述数组，和地址对应|
|opacity|数字	|	否	|0.8	|透明度，有效值为 0 到 1 之间|
|saveBtn|布尔	|	否	|true	|是否显示保存按键|
|rotateBtn|布尔 |	否	|ture	|是否显示旋转按键|
|circular|布尔 |	否	|false	|是否可以循环预览|
|reduction|布尔 |	否	|false	|是否还原图片才可切换（防止移动图片时出发切换事件）|
|longPress	|事件|	否	|空		|长按事件，返回对象：{src:地址,index:序号}|


## 使用方法
第一步，注册插件：
```javascript
	import previewImage from '@/components/kxj-previewImage/kxj-previewImage.vue';
	export default {
		components: { previewImage}, //注册插件
		data() {
			return {}
		}
	};
```
第二步，使用插件组件：
```vue
<template>
	<view>
		<previewImage ref="previewImage" :imgs="imgs" :descs="descs" @longPress="longPress"></previewImage>
	</view>
</template>
```
详见示例项目中的：pages/index/index.vue文件，关键代码都写有注解。

## 联系我
**本人邮箱：xiaohui.brook@foxmail.com**
如果插件使用上有什么问题、优化思路或其他需要帮助的地方，可邮箱联系我。


